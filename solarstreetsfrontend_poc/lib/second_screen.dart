import 'package:flutter/material.dart';
import 'dart:io';

class SecondScreen extends StatelessWidget {
  final File image;
  SecondScreen({Key key, @required this.image}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        const SizedBox(height: 16),
        Text(
          'Second Screen',
          textAlign: TextAlign.center,
        ),
        const SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Text(
            'THis is the second Screen',
            style: const TextStyle(
              fontSize: 12,
              color: Colors.grey,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }
}
