import 'package:flutter/material.dart';
import 'package:animations/animations.dart';
import 'package:google_fonts/google_fonts.dart';

import 'get_pic_widget.dart';
import 'second_screen.dart';
import 'colors.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  bool _canGoForward = true;

  void _incrementIndex() {
    setState(() {
      _selectedIndex += 1;
      _canGoForward = false;
    });
  }

  void _decrementIndex() {
    setState(() {
      _selectedIndex -= 1;
      _canGoForward = true;
    });
  }

  Widget _navigationBar() {
    return Container(
        color: primaryBlack,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FlatButton(
              disabledTextColor: Colors.grey,
              textColor: Colors.white,
              onPressed: _canGoForward ? null : _decrementIndex,
              child: Text('BACK'),
            ),
            RaisedButton(
              disabledTextColor: Colors.grey,
              textColor: Colors.white,
              color: Colors.deepPurple,
              splashColor: Colors.deepPurpleAccent,
              onPressed: _canGoForward ? _incrementIndex : null,
              child: Text('NEXT'),
            ),
          ],
        ));
  }

  Widget _transitionableContent() {
    return PageTransitionSwitcher(
      duration: const Duration(milliseconds: 300),
      reverse: _selectedIndex == 0,
      transitionBuilder: (
        Widget child,
        Animation<double> primaryAnimation,
        Animation<double> secondaryAnimation,
      ) {
        return SharedAxisTransition(
          child: child,
          animation: primaryAnimation,
          secondaryAnimation: secondaryAnimation,
          transitionType: SharedAxisTransitionType.horizontal,
        );
      },
      child: _selectedIndex == 0 ? GetPicWidget() : SecondScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Image.asset(
            'assets/logo_white.png',
          ),
        ),
        title: Text('Open Climate Fix', style: GoogleFonts.inter()),
      ),
      body: Container(
        child: SafeArea(
          child: Column(
            children: [
              Expanded(child: _transitionableContent()),
              _navigationBar(),
            ],
          ),
        ),
      ),
    );
  }
}
