import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';

class GetPicWidget extends StatefulWidget {
  @override
  _GetPicWidgetState createState() => _GetPicWidgetState();
}

class _GetPicWidgetState extends State<GetPicWidget> {
  File _image;
  Image backgroundImage;
  final picker = ImagePicker();

  Future getImage(imageSource) async {
    final pickedFile = await picker.getImage(source: imageSource);

    setState(() {
      _image = File(pickedFile.path);
      print(_image);
    });
  }

  void _removeImage() {
    _image.delete();
    setState(() {
      _image = null;
    });
  }

  Widget _buttonBar() {
    return ButtonBar(
        alignment: MainAxisAlignment.center,
        buttonHeight: 100,
        children: <Widget>[
          OutlineButton.icon(
              label: Text('Take a photo'),
              textColor: Theme.of(context).primaryColor,
              onPressed: () => getImage(ImageSource.camera),
              icon: Icon(
                Icons.add_a_photo,
              )),
          OutlineButton.icon(
              label: Text('Add from gallery'),
              textColor: Theme.of(context).primaryColor,
              onPressed: () => getImage(ImageSource.gallery),
              icon: Icon(
                Icons.add_photo_alternate,
              ))
        ]);
  }

  Widget _imageCard() {
    return Card(
        color: Colors.transparent,
        elevation: 0,
        borderOnForeground: true,
        child: Column(children: [
          ListTile(
            title: Text('Selected Image',
                style: TextStyle(fontWeight: FontWeight.w500)),
            subtitle: Text(DateTime.now().toString()),
            leading: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: FileImage(_image), fit: BoxFit.fitWidth)),
                height: 80,
                width: 80),
          ),
          FlatButton(
            child: const Text('REMOVE'),
            onPressed: () {
              _removeImage();
            },
          ),
        ]));
  }

  Widget _header() {
    return Container(
        margin: EdgeInsets.only(top: 50, bottom: 100, left: 10, right: 10),
        child: Text('Take or choose an image to upload to the OpenPV Map',
            style: Theme.of(context).textTheme.headline4));
  }

  @override
  void initState() {
    super.initState();
    backgroundImage = Image.asset('assets/background_large.jpg');
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    precacheImage(backgroundImage.image, context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: backgroundImage.image, fit: BoxFit.cover)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            _header(),
            _image == null ? _buttonBar() : _imageCard()
          ],
        ));
  }
}
